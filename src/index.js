import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from 'react-redux';

import App from "./App";
import store from './app/store';
import 'antd/dist/antd.css';        //ant design for styling all components correctly

                //hook our App onto our root div (index.js in public)
                //To use links and others we wrap it with Router
                //Provider, store are from redux for dealing state. Since we wrapped our App component inside Provider, every 
                        //component will have access to the variable store.
ReactDOM.render(
    <Router>
        <Provider store={ store }>         
            <App />
        </Provider>         
    </Router>, 
    document.getElementById('root')
);