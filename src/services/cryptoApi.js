import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const cryptoApiHeaders = {
    'X-RapidAPI-Host': 'coinranking1.p.rapidapi.com',
    'X-RapidAPI-Key': '53610cd091msh35a3be6d4f83d27p196381jsn33b7ed7f34fc'
};

const baseUrl = 'https://coinranking1.p.rapidapi.com';

//simple utility fn which simply adds url and header
const createRequest = (url) => ({ url,  headers: cryptoApiHeaders });

//utilize the createApi method and pass the options as an object
export const cryptoApi = createApi({
    reducerPath: 'cryptoApi',      //what is this reducer for? name
    baseQuery: fetchBaseQuery({ baseUrl }),
    endpoints: (builder) => ({
        getCryptos: builder.query({
            query: (count) => createRequest(`/coins?limit=${count}`),
        }),
        getCryptoDetails: builder.query({
            query: (coinId) => createRequest(`/coin/${coinId}`),
        }),
        getCryptoHistory: builder.query({
            query: ({ coinId, timeperiod }) => createRequest(`/coin/${coinId}/history?timePeriod=${timeperiod}`),
        }),
        getExchanges: builder.query({
            query: () => createRequest(`/exchanges`),
        })
    })
});

//redux toolkit creates a hook that you can call instantly to get all the data for your query.. 
    //They will also give loading states, finalised states, everything else you need while making API calls
export const {
    useGetCryptosQuery,
    useGetCryptoDetailsQuery,
    useGetCryptoHistoryQuery,
    useGetExchangesQuery,
} = cryptoApi;