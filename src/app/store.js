import { configureStore } from '@reduxjs/toolkit';

import { cryptoApi } from './../services/cryptoApi';
import { cryptoNewsApi } from '../services/cryptoNewsApi';

// call this as a function and pass an object inside of there
export default configureStore ({
    //params
    reducer: {
        [cryptoApi.reducerPath]: cryptoApi.reducer,
        [cryptoNewsApi.reducerPath]: cryptoNewsApi.reducer,
    },
})